<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home.index');
Route::post('/', 'HomeController@store')->name('home.store');
Route::get('/load-data', 'HomeController@loadData')->name('home.load-data');
Route::put('/', 'HomeController@update')->name('home.update');
Route::delete('/{id}', 'HomeController@destroy')->name('home.delete');
