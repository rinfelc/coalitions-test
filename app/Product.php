<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $table = 'products';

    protected $guarded = [];

    public function store( $data )
    {
        $row = [];
        $row['name'] = $data['name'];
        $row['stock'] = $data['stock'];
        $row['unit_price'] = $data['unit_price'];
        $row['total'] = $data['stock'] * $data['unit_price'];
        return Product::firstOrCreate( $row );
    }

    public function patch( $data, $id )
    {
        return Product::whereId( $id )
    						->update([
                                        'name' => $data['name'],
                                        'stock' => $data['stock'],
                                        'unit_price' => $data['unit_price'],
                                        'total' => $data['stock'] * $data['unit_price']
                            ]);
    }
}
