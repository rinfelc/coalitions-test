<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class HomeController extends Controller
{
    public $product;

    public function __construct( Product $product )
    {
        $this->product = $product;
    }

    public function index()
    {
        return view('home.index');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:products,name',
            'stock' => 'required|numeric|min:1',
            'unit_price' => 'required|numeric|min:1'
        ]);
        $this->product->store($request->all());
        return $this->product->orderBy('id', 'desc')->get();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
                'name' => 'required|unique:products,name,' . $request->id,
                'stock' => 'required|numeric|min:1',
                'unit_price' => 'required|numeric|min:1'
        ], ['name.unique' => 'This product is already entered']);
        $this->product->patch($request->all(), $request->id);
        return $this->product->find($request->id);
    }

    public function loadData()
    {
        return $this->product->orderBy('id', 'desc')->get();
    }

    public function destroy($id)
    {
        return $this->product->destroy($id);
    }
}
