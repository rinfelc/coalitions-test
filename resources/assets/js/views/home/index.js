pageVue = {
    methods: {
        addProduct () {
            axios.post('/', this.product)
            .then(response => {
                this.product.name = ''
                this.product.stock = ''
                this.product.unit_price = ''
                this.errors = []
                this.products = response.data
                this.$message.success('Products added successfully')
            })
            .catch(error => {
                this.errors = error.response.data.errors
                this.$message.error('Error: Please check input...')
            })
        },
        showProduct (row, index) {
            this.product.id = row.id
            this.product.name = row.name
            this.product.stock = row.stock
            this.product.unit_price = row.unit_price
            this.product.total = row.total
            this.newProduct = false
            this.productIndex = index
        },
        saveProduct () {
            axios.put('/', this.product)
            .then(response => {
                this.products[this.productIndex].name = response.data.name
                this.products[this.productIndex].stock = response.data.stock
                this.products[this.productIndex].unit_price = response.data.unit_price
                this.products[this.productIndex].total = response.data.total
                this.$message.success('Product Updated')
                this.clearField()
            })
            .catch(error => {
                this.errors = error.response.data.errors
                this.$message.error('Error: Please check input...')
            })
        },

        clearField () {
            this.product.id = ''
            this.product.name = ''
            this.product.stock = ''
            this.product.unit_price = ''
            this.product.total = ''
        },

        deleteProduct(row, index) {
            SweetAlert({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2498CC',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(() => {
                axios.delete('/' + row.id)
                    .then((response) => {
                        this.products.splice(index, 1)
                        this.$message.success('Product Deleted')
                    })
                    .catch((error) => {
                        this.$message.error('Error Deleting')
                    })
            }, function (dismiss) {
                // cancel handler
            });
        },
        loadData () {
            this.loadingTable = true
            axios.get('/load-data')
            .then(response => {
                this.products = response.data
                this.loadingTable = false
            })
        }
    },
    data () {
        return {
            productIndex: '',
            loadingTable: false,
            errors: [],
            products: [],
            product: {
                id: '',
                name: '',
                stock: '',
                unit_price: ''
            }
        }
    },
    mounted () {
        this.loadData()
    }
}