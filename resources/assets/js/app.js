
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'
locale.use(lang)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('form-error', require('./components/FormError.vue'));

if ((typeof pageVue == 'undefined') && (typeof masterVue == 'undefined')) {
    const app = new Vue({
        el: '#app'
    });
} else if (typeof masterVue == 'undefined') {
    const app = new Vue({
        el: '#app',
        mixins: [pageVue]
    });
} else if (typeof pageVue == 'undefined') {
    const app = new Vue({
        el: '#app',
        mixins: [masterVue]
    });
} else {
    const app = new Vue({
        el: '#app',
        mixins: [pageVue, masterVue]
    });
}
