@extends('layout.master')
@section('content')
    <h1 class="text-center">
        Product Info
    </h1>
    <div class="col-md-12 col-md-offset-6">
        <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Add Product</h3>
        </div>
        <div class="panel-body">
        <form>
            <div class="form-group">
                <label for="productName">Name</label>
                <input type="text" class="form-control" v-model="product.name" placeholder="Full Name of the Product">
                <form-error v-if="errors.name" :errors="errors">
                    <span v-for="title in errors.name">
                        @{{ title }}
                    </span>
                </form-error>
            </div>
            <div class="form-group">
                <label for="productStock">Stock</label>
                <input type="number" class="form-control" v-model="product.stock" placeholder="Stock Quantity in Number Only">
                <form-error v-if="errors.stock" :errors="errors">
                    <span v-for="title in errors.stock">
                        @{{ title }}
                    </span>
                </form-error>
            </div>
            <div class="form-group">
                <label for="productPrice">Unit Price ($/USD)</label>
                <input type="number" class="form-control" v-model="product.unit_price" placeholder="Unit Price in Number Only">
                <form-error v-if="errors.unit_price" :errors="errors">
                    <span v-for="title in errors.unit_price">
                        @{{ title }}
                    </span>
                </form-error>
            </div>
            <div class="form-group">
                <template  v-if="product.id">
                    <button class="btn btn-primary" @click.prevent="saveProduct">
                        Save Product
                    </button>
                    <button class="btn btn-default pull-right" @click.prevent="clearField">Clear Input Field</button>
                </template>
                <button class="btn btn-primary" @click.prevent="addProduct" v-else>
                    Add Product
                </button>
            </div>
        </form>
        </div>
        </div>
    </div>
    <div class="col-md-24">
        <el-table
            :data="products"
            stripe
            show-summary
            border
            v-loading="loadingTable"
            style="width: 100%">
            <el-table-column
            type="index"
            width="80">
            </el-table-column>
            <el-table-column
            label="Name"
            prop="name"
            show-overflow-tooltip
            width="360">
            </el-table-column>
            <el-table-column
            label="Stock"
            align="center"
            prop="stock"
            width="180">
            </el-table-column>
            <el-table-column
            label="Unit Price"
            align="center"
            prop="unit_price"
            width="180">
            </el-table-column>
            <el-table-column
            label="Total Price"
            align="center"
            prop="total"
            width="180">
            </el-table-column>
            <el-table-column label="Actions" align="center">
                <template scope="scope">
                    <el-button type="text" size="small" @click.prevent="showProduct(scope.row,scope.$index)">
                        Edit
                    </el-button>
                    <el-button type="text" size="small" @click.prevent="deleteProduct(scope.row,scope.$index)">
                        Delete
                    </el-button>
                </template>
            </el-table-column>
        </el-table>
    </div>
@endsection
@section('vue-page-script')
	<script src="{{ mix('views/home/index.min.js') }}"></script>
@endsection