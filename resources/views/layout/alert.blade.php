@if(Session::has('message'))
	@foreach(Session::get('message') as $type=>$message)
		<script>
			pageVue = {
				mounted() {
					this.$notify.{{$type}}({
		          		title: '{{ ucwords($type) }}',
		          		message: '{{ $message }}',
		          		type: '{{ $type }}'
		        	})
				}
			}
		</script>

	@endforeach
@endif